local M = {
    v = {
        ["J"] = { ":m '>+1<CR>gv=gv" },
        ["K"] = { ":m '<-2<CR>gv=gv" },
    },
    n = {
        ["<leader>P"] = { [["+p]], desc = "Paste from sys clipboard" },

        ["J"] = { "mzJ`z" },
        ["<C-d>"] = { "<C-d>zz" },
        ["<C-u>"] = { "<C-u>zz" },
        ["n"] = { "nzzzv" },
        ["N"] = { "Nzzzv" },

        ["Q"] = { "<nop>" },
        ["0"] = { "^", desc = "Go to first non blank character in line" },
        ["<leader>0"] = { "0", desc = "Go to true line beggining" },

        -- window movement
        ["<leader>ww"] = { "<C-w>w", desc = "Move between splits" },
        ["<leader>wc"] = { "<C-w>c", desc = "Close split" },
        ["<leader>wv"] = { "<C-w>v", desc = "Vertical split" },
        ["<leader>ws"] = { "<C-w>s", desc = "Horizontal split" },

        ["<leader><leader>"] = {
            function()
                if vim.bo.filetype ~= "lua" then
                    vim.notify("Not a lua file")
                    return
                end

                vim.cmd("so")
            end,
            desc = "Source current file",
        },

        ["<leader>ot"] = {
            function()
                require("taken.utils.themes").themeselector()
            end,
            desc = "Theme selector",
        },
    },
    vn = {
        ["<leader>+"] = { "<C-a>", desc = "Increment number" },
        ["<leader>-"] = { "<C-x>", desc = "Decrement number" },
        ["<leader>y"] = { [["+y]], desc = "Yank in to sys clipboard" },
        ["<leader>d"] = { [["_d]], desc = "Actually deletes text" },
    },
}

require("taken.utils.maps").setmap(M)
