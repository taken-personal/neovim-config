local configDir = vim.fn.stdpath("config")
local prefs = configDir .. "/lua/taken/prefs.lua"
local example = configDir .. "/lua/taken/prefs.lua.example"

if vim.fn.filereadable(prefs) == 0 then
    vim.cmd("silent! !cp " .. example .. " " .. prefs)
end
