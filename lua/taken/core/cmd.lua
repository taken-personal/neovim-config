local themefuncs = require("taken.utils.themefuncs")

-- useful variables
local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup
local cmd = vim.api.nvim_create_user_command
-- Groups
local nvimhelp = augroup("help_window_right", { clear = true })
local mkdirrun = augroup("MkdirRun", { clear = true })
local yank = augroup("kickstart-highlight-yank", { clear = true })
local filechange = augroup("filechange", { clear = true })

cmd("SetTheme", function(arg)
    local themeselected = arg.fargs[1]

    local getallthemes = themefuncs.getAllThemes()
    for _, theme in ipairs(getallthemes) do
        if theme == themeselected then
            vim.cmd("colorscheme " .. themeselected)
            themefuncs.applytofile(themeselected)
        end
    end
end, {
    desc = "Select a theme",
    nargs = 1,
    complete = function()
        local getallthemes = themefuncs.getAllThemes()
        return getallthemes
    end,
})

autocmd("TextYankPost", {
    desc = "Highlight when yanking (copying) text",
    group = yank,
    callback = function()
        vim.highlight.on_yank()
    end,
})

autocmd("BufWinEnter", {
    group = nvimhelp,
    pattern = { "*.txt" },
    callback = function()
        if vim.o.filetype == "help" then
            vim.cmd.wincmd("L")
        end
    end,
})

autocmd("BufWritePre", {
    group = mkdirrun,
    pattern = { "*" },
    callback = function()
        require("taken.utils.mkdir").run()
    end,
})

autocmd("FileChangedShell", {
    group = filechange,
    pattern = { "*" },
    callback = function(args)
        --- @type string
        local file = args.file
        local cwd = vim.fn.getcwd()
        if file:find(cwd) then
            file = file:sub(#cwd + 2)
        end
        vim.notify(file .. " has been chnaged", vim.log.levels.WARN)
    end,
})
