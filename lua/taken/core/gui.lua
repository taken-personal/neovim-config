vim.o.guifont = "CaskaydiaCove Nerd Font Mono:h16"

-- neovide
vim.g.neovide_transparency = 0.8
vim.g.neovide_floating_blur_amount_x = 2.0
vim.g.neovide_floating_blur_amount_y = 2.0
vim.g.neovide_refresh_rate = 165
vim.g.neovide_refresh_rate_idle = 5
vim.g.neovide_cursor_vfx_mode = "railgun"
vim.g.neovide_cursor_smooth_blink = true

local neovideDir = "C:\\Program Files\\Neovide"
if vim.g.neovide and vim.fn.getcwd() == neovideDir then
    vim.cmd([[cd ~]])
end

-- goneovim
local home = os.getenv("HOME")
local goneovimDir = home .. "\\scoop\\apps\\goneovim\\current"

if vim.g.goneovim and vim.fn.getcwd() == goneovimDir then
    vim.cmd([[cd ~]])
end
