local g = vim.g
local o = vim.o
local opt = vim.opt

opt.fillchars = "eob: "

g.mapleader = " "
g.maplocalleader = "."

-- opt.cursorline = true
-- opt.cursorcolumn = true

opt.foldmethod = "expr"
opt.foldexpr = "nvim_treesitter#foldexpr()"
vim.opt.foldenable = false

opt.nu = true
opt.relativenumber = true

opt.tabstop = 4
opt.softtabstop = 4
opt.shiftwidth = 4
opt.expandtab = true

opt.swapfile = false
opt.backup = false
opt.undofile = true
opt.undodir = vim.fn.expand("~") .. "/.vim/undodir"

opt.smartindent = true
opt.wrap = false

opt.ignorecase = true
opt.smartcase = true

opt.hlsearch = false
opt.incsearch = true

opt.termguicolors = true

opt.splitright = true
opt.splitbelow = true

opt.scrolloff = 8
opt.signcolumn = "yes"
opt.isfname:append("@-@")
opt.backspace = "indent,eol,start"

opt.updatetime = 50

-- windows
if vim.fn.has("win32") == 1 then
    o.shell = "pwsh -nologo"
    o.shellcmdflag =
        "-NoLogo -NoProfile -ExecutionPolicy RemoteSigned -Command [Console]::InputEncoding=[Console]::OutputEncoding=[System.Text.Encoding]::UTF8;"
    o.shellredir = "2>&1 | Out-File -Encoding UTF8 %s; exit $LastExitCode"
    o.shellpipe = "2>&1 | Out-File -Encoding UTF8 %s; exit $LastExitCode"
    o.shellquote = ""
    o.shellxquote = ""
end
