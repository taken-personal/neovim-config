-- maps
--- @class MappingOpts
--- @field [1] string
--- @field desc string

--- @alias Mapping table<string, MappingOpts>

--- @class Maps
--- @field v Mapping
--- @field n Mapping
--- @field vn Mapping

-- themefuncs
--- @alias Theme string
--- @class Themes table<Theme>
