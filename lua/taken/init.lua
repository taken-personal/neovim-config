if vim.g.vscode then
    require("taken.core.remaps")
else
    require("taken.core")
    require("taken.lazy")
    require("taken.custom")
end
