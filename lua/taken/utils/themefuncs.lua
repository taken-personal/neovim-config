local oldstring = 'M.colorscheme = .*"'
local configDir = vim.fn.stdpath("config")
local prefsFile = configDir .. "/lua/taken/prefs.lua"

--- @return Themes
local getAllThemes = function()
    vim.api.nvim_exec_autocmds("User", { pattern = "ThemeSwitcher", modeline = false })

    local colors = vim.fn.globpath(vim.o.rtp, "colors/*", true, 1)
    local dataDir = vim.fn.stdpath("data")
    ---@diagnostic disable-next-line: param-type-mismatch
    local newpath = string.gsub(dataDir, "%-", "%%-")
    local themes = {}

    for _, v in ipairs(colors) do
        if string.find(v, "lazy") then
            local filename = ""
            if vim.loop.os_uname().sysname == "Windows_NT" then
                filename = string.gsub(v, newpath .. "\\lazy\\.*\\", "")
            else
                filename = string.gsub(v, newpath .. "/lazy/.*/", "")
            end
            local themename = string.gsub(filename, ".vim", "")
            local themename2 = string.gsub(themename, ".lua", "")

            table.insert(themes, themename2)
        end
    end

    return themes
end

--- change theme
--- @param old string
--- @param new string
local change_theme = function(old, new)
    -- function from nvchad
    local file = io.open(prefsFile, "r")

    if file == nil then
        vim.notify("Could not open " .. prefsFile, 4)
        return
    end

    local added_pattern = string.gsub(old, "-", "%%-")
    local new_content = file:read("*all"):gsub(added_pattern, new)

    file = io.open(prefsFile, "w")

    if file == nil then
        vim.notify("Could not open " .. prefsFile, 4)
        return
    end

    file:write(new_content)
    file:close()
end

--- applytofile
--- @param choice string
local applytofile = function(choice)
    if choice == nil then
        vim.notify("No theme selected!", 4)
        return
    end

    vim.cmd("colorscheme " .. choice)
    local newstring = 'M.colorscheme = "' .. choice .. '"'
    change_theme(oldstring, newstring)

    local cursorcolumncolor = vim.fn.synIDattr(vim.fn.hlID("CursorColumn"), "bg")
    if cursorcolumncolor ~= "" then
        vim.cmd("hi CursorLine guibg=" .. cursorcolumncolor)
    end
end

return {
    getAllThemes = getAllThemes,
    change_theme = change_theme,
    applytofile = applytofile,
}
