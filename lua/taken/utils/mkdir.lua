-- code by https://github.com/jghauser/mkdir.nvim

local fn = vim.fn

local M = {}

function M.run()
    local dir = fn.expand("<afile>:p:h")

    if dir:find("%l+://") == 1 then
        return
    end

    if fn.isdirectory(dir) == 0 then
        fn.mkdir(dir, "p")
    end
end

return M
