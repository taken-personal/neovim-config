local M = {}

function M.themeselector(opts)
    local themefuncs = require("taken.utils.themefuncs")
    local pickers = require("telescope.pickers")
    local finders = require("telescope.finders")
    local conf = require("telescope.config").values
    local actions = require("telescope.actions")
    local action_state = require("telescope.actions.state")
    -- local action_set = require("telescope.actions.set")

    local themes = themefuncs.getAllThemes()
    local applytofile = themefuncs.applytofile

    opts = opts or {}
    pickers
        .new(opts, {
            prompt_title = "Colorscheme",
            finder = finders.new_table({
                results = themes,
            }),
            sorter = conf.generic_sorter(opts),
            attach_mappings = function(prompt_bufnr)
                actions.select_default:replace(function()
                    actions.close(prompt_bufnr)
                    local selection = action_state.get_selected_entry()
                    applytofile(selection.value)
                end)
                -- action_set.shift_selection:enhance({
                --     post = function()
                --         local selection = action_state.get_selected_entry()
                --         if selection == nil then
                --             vim.notify("Error while selecting theme", vim.log.levels.ERROR)
                --         end
                --         vim.cmd("colorscheme " .. selection.value)
                --     end,
                -- })
                -- actions.close:enhance({
                --     post = function()
                --         local selection = action_state.get_selected_entry()
                --         if selection == nil then
                --             vim.notify("Error while selecting theme", vim.log.levels.ERROR)
                --         end
                --         exec(selection.value)
                --     end,
                -- })
                return true
            end,
        })
        :find()
end

return M
