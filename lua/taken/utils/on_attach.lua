local on_attach = function(client, bufnr)
    local function opts(desc)
        if desc then
            return { silent = true, buffer = bufnr, desc = desc }
        else
            return { silent = true, buffer = bufnr }
        end
    end

    vim.keymap.set("n", "K", function()
        vim.lsp.buf.hover()
    end, opts("Show hover information"))
    vim.keymap.set("n", "gd", "<cmd>Telescope lsp_definitions<CR>", opts("Find definitions"))
    vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts("Find declaration"))
    vim.keymap.set("n", "gi", "<cmd>Telescope lsp_implementations<CR>", opts("Find implementations"))
    vim.keymap.set("n", "gt", "<cmd>Telescope lsp_type_definitions<CR>", opts("Find type definitions"))
    vim.keymap.set("n", "gR", "<cmd>Telescope lsp_references<CR>", opts("Find references"))
    vim.keymap.set("n", "<leader>vws", function()
        vim.lsp.buf.workspace_symbol()
    end, opts("Search workspace symbols"))
    vim.keymap.set("n", "<leader>vd", function()
        vim.diagnostic.open_float()
    end, opts("Open diagnostics"))
    -- vim.keymap.set("n", "<leader>vca", function()
    --     vim.lsp.buf.code_action()
    -- end, opts("Get code actions"))
    vim.keymap.set("n", "<leader>vca", function()
        require("tiny-code-action").code_action()
    end, opts("Get code actions"))
    vim.keymap.set("n", "<leader>vr", function()
        return ":IncRename " .. vim.fn.expand("<cword>")
    end, { expr = true, silent = true, buffer = bufnr, desc = "Rename symbol" })
    vim.keymap.set("n", "<leader>vn", function()
        vim.lsp.buf.rename()
    end, opts("Rename symbol"))
    vim.keymap.set("n", "<leader>vh", function()
        if vim.fn.has("nvim-0.10.0") == 0 then
            vim.notify("Inlay hints are only available in Neovim 0.10.0 and above", vim.log.levels.ERROR)
            return
        end

        if client.server_capabilities.inlayHintProvider then
            local current_state = vim.lsp.inlay_hint.is_enabled({ bufnr = bufnr })
            vim.lsp.inlay_hint.enable(not current_state, { bufnr = bufnr })
        end
    end, opts("Toggle inlay hints"))
    vim.keymap.set("n", "[d", function()
        vim.diagnostic.goto_next()
    end, opts("Go to next diagnostic"))
    vim.keymap.set("n", "]d", function()
        vim.diagnostic.goto_prev()
    end, opts("Go to previous diagnostic"))
    vim.keymap.set("i", "<C-h>", function()
        vim.lsp.buf.signature_help()
    end, opts("Show signature help"))
end

return on_attach
