local M = {}
local set = vim.keymap.set

--- @param maps Maps
function M.setmap(maps)
    local v = maps.v or {}
    local n = maps.n or {}
    local vn = maps.vn or {}

    for map, command in pairs(v) do
        if command.desc then
            set("v", map, command[1], { desc = command.desc, silent = true })
        else
            set("v", map, command[1], { silent = true })
        end
    end

    for map, command in pairs(n) do
        if command.desc then
            set("n", map, command[1], { desc = command.desc, silent = true })
        else
            set("n", map, command[1], { silent = true })
        end
    end

    for map, command in pairs(vn) do
        if command.desc then
            set({ "v", "n" }, map, command[1], { desc = command.desc, silent = true })
        else
            set({ "v", "n" }, map, command[1], { silent = true })
        end
    end
end

return M
