--- @type LazyPluginSpec
return {
    "rcarriga/nvim-dap-ui",
    dependencies = {
        "mfussenegger/nvim-dap",
        "nvim-neotest/nvim-nio",
    },
    config = function()
        local dap = require("dap")
        local dapui = require("dapui")

        dap.adapters["pwa-node"] = {
            type = "server",
            host = "localhost",
            port = "8123",
            executable = {
                command = "js-debug-adapter",
            },
        }
        dap.configurations.javascript = {
            {
                name = "Attach to process (global nvim)",
                type = "pwa-node",
                request = "attach",
                port = 9229,
            },
        }
        dap.configurations.typescript = {
            {
                name = "Attach to process (global nvim)",
                type = "pwa-node",
                request = "attach",
                port = 9229,
            },
        }

        dap.listeners.after.event_initialized["dapui_config"] = function()
            dapui.open()
        end
        dap.listeners.before.event_terminated["dapui_config"] = function()
            dapui.close()
        end
        dap.listeners.before.event_exited["dapui_config"] = function()
            dapui.close()
        end

        dapui.setup()

        vim.keymap.set("n", "<leader>cb", "<cmd>DapToggleBreakpoint<CR>", { silent = true, desc = "Toggle breakpoint" })
        vim.keymap.set("n", "<leader>cc", "<cmd>DapContinue<CR>", { silent = true, desc = "Continue debugger" })
        vim.keymap.set("n", "<leader>ci", "<cmd>DapStepInto<CR>", { silent = true, desc = "Step into" })
        vim.keymap.set("n", "<leader>co", "<cmd>DapStepOver<CR>", { silent = true, desc = "Step over" })
        vim.keymap.set("n", "<leader>cO", "<cmd>DapStepOut<CR>", { silent = true, desc = "Step out" })
        vim.keymap.set("n", "<leader>ce", "<cmd>DapEval", { silent = true, desc = "Evaluate expression" })
    end,
}
