--- @type LazyPluginSpec
return {
    "luckasRanarison/tailwind-tools.nvim",
    name = "tailwind-tools",
    build = ":UpdateRemotePlugins",
    ft = { "html", "svelte", "astro", "vue", "typescriptreact", "php", "blade" },
    dependencies = {
        "nvim-treesitter/nvim-treesitter",
        "nvim-telescope/telescope.nvim",
        "neovim/nvim-lspconfig",
        "razak17/tailwind-fold.nvim",
    },
    config = function()
        local on_attach = require("taken.utils.on_attach")

        require("tailwind-tools").setup({
            server = {
                on_attach = on_attach,
            },
            extension = {
                queries = { "tsx " },
            },
        })

        require("tailwind-fold").setup({
            symbol = "󱏿",
        })
    end,
}
