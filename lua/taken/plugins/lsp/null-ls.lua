--- @type LazyPluginSpec
return {
    "nvimtools/none-ls.nvim",
    event = { "BufReadPre", "BufNewFile" },
    enabled = false,
    config = function()
        local null_ls = require("null-ls")

        local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
        local formatting = null_ls.builtins.formatting

        local root_has_file = function(files)
            return function(utils)
                return utils.root_has_file(files)
            end
        end
        local prettier_root_files = {
            ".prettierrc",
            ".prettierrc.json",
            ".prettierrc.js",
            ".prettierrc.mjs",
            ".prettierrc.cjs",
            "prettier.config.cjs",
            "prettier.config.js",
            "prettier.config.mjs",
        }
        local formatting_with_prettier = formatting.prettier.with({
            condition = root_has_file(prettier_root_files),
        })

        null_ls.setup({
            sources = {
                formatting.stylua,
                formatting.prettier.with(formatting_with_prettier),
            },
            on_attach = function(client, bufnr)
                if client.supports_method("textDocument/formatting") then
                    vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
                    vim.api.nvim_create_autocmd("BufWritePre", {
                        group = augroup,
                        buffer = bufnr,
                        callback = function()
                            vim.lsp.buf.format({ async = false })
                        end,
                    })
                end
            end,
        })
    end,
}
