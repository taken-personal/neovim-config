--- @type LazyPluginSpec
return {
    "neovim/nvim-lspconfig",
    dependencies = {
        { "antosha417/nvim-lsp-file-operations", config = true },
        -- "hrsh7th/cmp-nvim-lsp",
        "saghen/blink.cmp",
        "nvim-lua/plenary.nvim",
        "pmizio/typescript-tools.nvim",
        "dmmulroy/ts-error-translator.nvim",
    },
    event = { "BufReadPre", "BufNewFile" },
    config = function()
        local lspconfig = require("lspconfig")
        local tserrortranslator = require("ts-error-translator")
        local util = require("lspconfig.util")
        local on_attach = require("taken.utils.on_attach")
        local path = util.path

        -- local cmp_nvim_lsp = require("cmp_nvim_lsp")
        -- local capabilities = cmp_nvim_lsp.default_capabilities()

        local blink = require("blink.cmp")
        local capabilities = blink.get_lsp_capabilities()

        local signs = { Error = " ", Warn = " ", Hint = "ﴞ ", Info = " " }
        for type, icon in pairs(signs) do
            local hl = "DiagnosticSign" .. type
            vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
        end

        local defaultLsps = {
            "html",
            "cssls",
            "jsonls",
            "rust_analyzer",
            "yamlls",
            "bashls",
            "marksman",
            "eslint",
        }

        for _, lsp in ipairs(defaultLsps) do
            lspconfig[lsp].setup({
                capabilities = capabilities,
                on_attach = on_attach,
            })
        end

        -- lua
        lspconfig["lua_ls"].setup({
            capabilities = capabilities,
            on_attach = on_attach,
            settings = {
                Lua = {
                    hint = {
                        enable = true,
                    },
                    runtime = {
                        version = "LuaJIT",
                    },
                    diagnostics = {
                        globals = { "vim" },
                    },
                    completion = {
                        callSnippet = "Replace",
                    },
                    format = {
                        enable = false,
                    },
                },
            },
        })

        local function get_python_path(workspace)
            -- Use activated virtualenv.
            if vim.env.VIRTUAL_ENV then
                return path.join(vim.env.VIRTUAL_ENV, "bin", "python")
            end

            -- Find and use virtualenv in workspace directory.
            for _, pattern in ipairs({ "*", ".*" }) do
                local match = vim.fn.glob(path.join(workspace, pattern, "pyvenv.cfg"))
                if match ~= "" then
                    return path.join(path.dirname(match), "bin", "python")
                end
            end

            return nil
        end

        -- python
        lspconfig["pyright"].setup({
            capabilities = capabilities,
            on_attach = on_attach,
            before_init = function(_, config)
                local python_path = get_python_path(config.root_dir)
                if python_path == nil then
                    return
                end
                config.settings.python.pythonPath = get_python_path(config.root_dir)
            end,
        })

        -- typescript
        lspconfig["ts_ls"].setup({
            capabilities = capabilities,
            on_attach = on_attach,
            settings = {
                typescript = {
                    inlayHints = {
                        includeInlayParameterNameHints = "all",
                        includeInlayParameterNameHintsWhenArgumentMatchesName = true,
                        includeInlayFunctionParameterTypeHints = true,
                        includeInlayVariableTypeHints = true,
                        includeInlayVariableTypeHintsWhenTypeMatchesName = true,
                        includeInlayPropertyDeclarationTypeHints = true,
                        includeInlayFunctionLikeReturnTypeHints = true,
                        includeInlayEnumMemberValueHints = true,
                    },
                },
                javascript = {
                    inlayHints = {
                        includeInlayParameterNameHints = "all",
                        includeInlayParameterNameHintsWhenArgumentMatchesName = true,
                        includeInlayFunctionParameterTypeHints = true,
                        includeInlayVariableTypeHints = true,
                        includeInlayVariableTypeHintsWhenTypeMatchesName = true,
                        includeInlayPropertyDeclarationTypeHints = true,
                        includeInlayFunctionLikeReturnTypeHints = true,
                        includeInlayEnumMemberValueHints = true,
                    },
                },
            },
        })

        -- powershell
        lspconfig["powershell_es"].setup({
            capabilities = capabilities,
            on_attach = on_attach,
            bundle_path = vim.fn.stdpath("data") .. "/mason/packages/powershell-editor-services",
        })

        tserrortranslator.setup()
    end,
}
