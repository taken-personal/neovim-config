--- @type LazyPluginSpec
return {
    "j-hui/fidget.nvim",
    event = "LspAttach",
    config = function()
        vim.api.nvim_set_hl(0, "FidgetTitle", { link = "DiagnosticText" })
        vim.api.nvim_set_hl(0, "FidgetTask", { link = "DiagnosticInfo" })

        require("fidget").setup({
            notification = {
                window = {
                    winblend = 0,
                    relative = "editor",
                },
            },
        })
    end,
}
