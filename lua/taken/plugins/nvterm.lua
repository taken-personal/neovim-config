--- @type LazyPluginSpec
return {
    "NvChad/nvterm",
    keys = {
        {
            "<A-t>",
            function()
                require("nvterm.terminal").toggle("float")
            end,
            desc = "Open float terminal",
        },
        {
            "<A-T>",
            function()
                require("nvterm.terminal").toggle("horizontal")
            end,
            desc = "Open hortizontal terminal",
        },
    },
    config = function()
        local nvterm = require("nvterm")

        nvterm.setup({
            terminals = {
                type_opts = {
                    float = {
                        row = 0.1,
                        col = 0.1,
                        width = 0.8,
                        height = 0.8,
                    },
                },
            },
        })
    end,
}
