--- @type LazyPluginSpec
return {
    "MeanderingProgrammer/render-markdown.nvim",
    ft = "markdown",
    dependencies = { "nvim-treesitter/nvim-treesitter", "nvim-tree/nvim-web-devicons" },
    config = function()
        require("render-markdown").setup({
            heading = {
                width = "block",
            },
            code = {
                sign = true,
                width = "block",
            },
        })
    end,
}
