--- @type LazyPluginSpec
return {
    "rainbowhxch/beacon.nvim",
    event = { "BufReadPre", "BufNewFile" },
    config = function()
        local beacon = require("beacon")

        beacon.setup({
            enable = true,
            size = 60,
            fade = true,
            minimal_jump = 10,
            show_jumps = true,
            focus_gained = false,
            shrink = true,
            timeout = 750,
            ignore_buffers = {},
            ignore_filetypes = {
                "NeogitStatus",
            },
        })
    end,
}
