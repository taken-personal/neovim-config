--- @type LazyPluginSpec
return {
    "michaelrommel/nvim-silicon",
    cmd = "Silicon",
    keys = {
        {
            "<leader>oc",
            ":Silicon<CR>",
            mode = { "v", "n" },
            desc = "Capture code to image",
        },
    },
    config = function()
        -- local tmpdir = vim.fn.environ().TMP .. "/"

        require("nvim-silicon").setup({
            font = "CaskaydiaCove Nerd Font Mono=34",
            theme = "OneHalfDark",
            -- background = "#076678",
            -- background_image = nil,
            gobble = true,
            num_separator = " ",
            no_line_number = true,
            -- output = function()
            --     return tmpdir .. os.date("!%Y-%m-%dT%H-%M-%S") .. "_code.png"
            -- end,
            output = nil,
            to_clipboard = true,
            window_title = function()
                return vim.fn.fnamemodify(vim.api.nvim_buf_get_name(vim.api.nvim_get_current_buf()), ":t")
                    .. " | Silicon & nvim-silicon"
            end,
        })
    end,
}
