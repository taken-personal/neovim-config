--- @type LazyPluginSpec
return {
    "xiyaowong/transparent.nvim",
    config = function()
        local transparent = require("transparent")

        vim.keymap.set("n", "<leader>tt", "<cmd>TransparentToggle<CR>", { silent = true, desc = "Toggle transparency" })

        transparent.setup({
            groups = {
                "Normal",
                "NormalNC",
                "FloatBorder",
                "Comment",
                "Constant",
                "Special",
                "Identifier",
                "Statement",
                "PreProc",
                "Type",
                "Underlined",
                "Todo",
                "String",
                "Function",
                "Conditional",
                "Repeat",
                "Operator",
                "Structure",
                "LineNr",
                "NonText",
                "SignColumn",
                "CursorLineNr",
                "EndOfBuffer",
            },
            extra_groups = {
                "NormalSB",
                "Folded",
                "NonText",
                "SpecialKey",
                "VertSplit",
                "EndOfBuffer",
                "SignColumn",
                "NotifyTRACEBorder",
                "NotifyERRORBorder",
                "NotifyDEBUGBorder",
                "NotifyWARNBorder",
                "NotifyINFOBorder",
            },
            exclude_groups = {},
        })
    end,
}
