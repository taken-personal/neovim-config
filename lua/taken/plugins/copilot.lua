--- @type LazyPluginSpec
return {
    "zbirenbaum/copilot.lua",
    cmd = "Copilot",
    event = "InsertEnter",
    dependencies = "AndreM222/copilot-lualine",
    config = function()
        require("copilot").setup({
            panel = {
                keymap = {
                    open = "<M-c>",
                },
            },
            suggestion = {
                auto_trigger = true,
                keymap = {
                    accept = "<M-CR>",
                },
            },
            filetypes = {
                lua = true,
                javascript = true,
                python = true,
                markdown = true,
                json = true,
                sh = function()
                    if string.match(vim.fs.basename(vim.api.nvim_buf_get_name(0)), "^%.env.*") then
                        return false
                    end
                    return true
                end,
                ["grug-far"] = false,
            },
        })
    end,
}
