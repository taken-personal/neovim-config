--- @type LazyPluginSpec
return {
    "linrongbin16/gitlinker.nvim",
    cmd = {
        "GitLink",
    },
    keys = {
        {
            "<leader>gl",
            "<cmd>GitLink remote=origin<CR>",
            mode = { "v", "n" },
            desc = "Copies git link to highlighted code",
        },
        {
            "<leader>gL",
            "<cmd>GitLink! remote=origin<CR>",
            mode = { "v", "n" },
            desc = "Opens git link to highlighted code",
        },
        {
            "<leader>gb",
            "<cmd>GitLink blame<CR>",
            mode = { "v", "n" },
            desc = "Copies git blame link to highlighted code",
        },
        {
            "<leader>gB",
            "<cmd>GitLink! blame<CR>",
            mode = { "v", "n" },
            desc = "Opens git blame link to highlighted code",
        },
    },
    config = function()
        local gitlinker = require("gitlinker")

        gitlinker.setup()
    end,
}
