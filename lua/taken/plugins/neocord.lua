--- @type LazyPluginSpec
return {
    "IogaMaster/neocord",
    config = function()
        local neocord = require("neocord")

        neocord.setup({
            logo = "neovim",
            logo_tooltip = "The One True Text Editor",
            main_image = "neovim",
            client_id = "793271441293967371",
            log_level = nil,
            debounce_timeout = 10,
            blacklist = {},
            file_assets = {},
            show_time = true,
            global_timer = true,

            editing_text = "Editing %s",
            file_explorer_text = "Browsing %s",
            git_commit_text = "Committing changes",
            plugin_manager_text = "Managing plugins",
            reading_text = "Reading %s",
            workspace_text = "Working on %s",
            line_number_text = "Line %s out of %s",
            terminal_text = "Using Terminal",
        })
    end,
}
