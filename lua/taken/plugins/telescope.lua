--- @type LazyPluginSpec
return {
    "nvim-telescope/telescope.nvim",
    branch = "0.1.x",
    dependencies = {
        "nvim-lua/plenary.nvim",
        "nvim-telescope/telescope-file-browser.nvim",
        { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
    },
    config = function()
        local telescope = require("telescope")

        local command = { "fd", "-H", "--type", "file" }
        local ignored = { ".git" }

        for _, v in ipairs(ignored) do
            table.insert(command, "--exclude")
            table.insert(command, v)
        end

        telescope.setup({
            defaults = {
                path_display = { "smart" },
            },
            pickers = {
                find_files = {
                    find_command = command,
                },
            },
            extensions = {
                file_browser = {
                    hidden = {
                        file_browser = true,
                        folder_browser = true,
                    },
                },
                fzf = {
                    fuzzy = true, -- false will only do exact matching
                    override_generic_sorter = true, -- override the generic sorter
                    override_file_sorter = true, -- override the file sorter
                    case_mode = "smart_case", -- or "ignore_case" or "respect_case"
                },
            },
        })

        telescope.load_extension("file_browser")
        telescope.load_extension("fzf")

        vim.keymap.set("n", "<leader>ff", "<cmd>Telescope find_files<CR>", { silent = true, desc = "Find files" })
        vim.keymap.set("n", "<leader>fg", "<cmd>Telescope live_grep<CR>", { silent = true, desc = "Live grep" })
        vim.keymap.set("n", "<leader>fh", "<cmd>Telescope help_tags<CR>", { silent = true, desc = "Help tags" })
        vim.keymap.set("n", "<leader>fd", "<cmd>Telescope file_browser<CR>", { silent = true, desc = "File browser" })
    end,
}
