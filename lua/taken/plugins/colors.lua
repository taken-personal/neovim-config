--- @type LazyPluginSpec[]
return {
    {
        "olivercederborg/poimandres.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
        config = true,
    },
    {
        "2giosangmitom/nightfall.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
        config = true,
    },
    {
        "tiagovla/tokyodark.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "Zeioth/neon.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "Yazeed1s/oh-lucy.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "Mofiqul/vscode.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "maxmx03/fluoromachine.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "samharju/synthweave.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "b0o/lavi.nvim",
        dependencies = { "rktjmp/lush.nvim" },
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "pauchiner/pastelnight.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "JoosepAlviste/palenightfall.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "EdenEast/nightfox.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
        config = function()
            local nightfox = require("nightfox")

            nightfox.setup({
                options = {
                    styles = {
                        comments = "italic",
                        functions = "italic",
                    },
                },
            })
        end,
    },
    {
        "rose-pine/neovim",
        name = "rose-pine",
        priority = 1000,
        event = "User ThemeSwitcher",
        config = function()
            local rose_pine = require("rose-pine")
            rose_pine.setup({
                variant = "dawn",
                dark_variant = "main",
            })
        end,
    },
    {
        "bluz71/vim-nightfly-colors",
        name = "nightfly",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "catppuccin/nvim",
        name = "catppuccin",
        priority = 1000,
        event = "User ThemeSwitcher",
        config = function()
            local catppuccin = require("catppuccin")
            catppuccin.setup({
                integrations = { notify = true, mason = true },
            })
        end,
    },
    {
        "folke/tokyonight.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "Mofiqul/dracula.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "roflolilolmao/oceanic-next.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "olimorris/onedarkpro.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "NTBBloodbath/doom-one.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "shaunsingh/moonlight.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "kvrohit/mellow.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "ofirgall/ofirkai.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
    {
        "rebelot/kanagawa.nvim",
        priority = 1000,
        event = "User ThemeSwitcher",
    },
}
