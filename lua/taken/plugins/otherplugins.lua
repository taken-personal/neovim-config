--- @type LazyPluginSpec[]
return {
    {
        "smjonas/inc-rename.nvim",
        config = true,
    },
    {
        "MagicDuck/grug-far.nvim",
        config = true,
        cmd = "GrugFar",
    },
    {
        "Bekaboo/dropbar.nvim",
        enabled = false,
        dependencies = {
            "nvim-telescope/telescope-fzf-native.nvim",
        },
    },
    {
        "OXY2DEV/markview.nvim",
        enabled = false,
        dependencies = {
            "nvim-tree/nvim-web-devicons",
        },
    },
    {
        "OXY2DEV/helpview.nvim",
        dependencies = {
            "nvim-treesitter/nvim-treesitter",
        },
    },
    {
        "GustavEikaas/code-playground.nvim",
        cmd = "Code",
        config = function()
            require("code-playground").setup()
        end,
    },
    {
        "miversen33/sunglasses.nvim",
        config = true,
        event = "UIEnter",
    },
    {
        "zeioth/garbage-day.nvim",
        dependencies = "neovim/nvim-lspconfig",
        event = "VeryLazy",
        config = true,
    },
    {
        "wakatime/vim-wakatime",
        name = "Wakatime",
        lazy = false,
    },
    {
        "folke/todo-comments.nvim",
        ependencies = { "nvim-lua/plenary.nvim" },
        event = { "BufReadPre", "BufNewFile" },
        config = function()
            local todo = require("todo-comments")

            todo.setup({})
            vim.keymap.set("n", "<leader>ft", "<cmd>TodoTelescope<CR>", { silent = true, desc = "Search for TODOs" })
        end,
    },
    {
        "mbbill/undotree",
        cmd = "UndotreeToggle",
        keys = {
            { "<leader>tu", "<cmd>UndotreeToggle<CR>", desc = "Toggle undotree" },
        },
    },
    {
        "sindrets/diffview.nvim",
        cmd = "DiffviewOpen",
    },
    {
        "windwp/nvim-ts-autotag",
        dependencies = "nvim-treesitter",
        event = { "BufReadPre", "BufNewFile" },
        config = function()
            require("nvim-ts-autotag").setup()
        end,
    },
    {
        "szw/vim-maximizer",
        cmd = "MaximizerToggle",
        keys = {
            { "<leader>tf", "<cmd>MaximizerToggle <CR>", desc = "Toggle maximizer" },
        },
    },
    {
        "NvChad/nvim-colorizer.lua",
        event = { "BufReadPre", "BufNewFile" },
        config = true,
    },
    {
        "kylechui/nvim-surround",
        version = "*",
        event = "VeryLazy",
        config = true,
    },
    {
        "iamcco/markdown-preview.nvim",
        cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
        ft = "markdown",
        keys = {
            { "<leader>tm", "<cmd>MarkdownPreviewToggle <CR>", desc = "Toggle markdown preview" },
        },
        build = function()
            vim.fn["mkdp#util#install"]()
        end,
        config = function()
            vim.cmd([[
            let g:mkdp_auto_close = 0
            let g:mkdp_theme = 'dark'
            ]])
        end,
    },
    {
        "akinsho/git-conflict.nvim",
        version = "*",
        config = function()
            local git_conflict = require("git-conflict")

            git_conflict.setup()

            vim.api.nvim_create_autocmd("User", {
                pattern = "GitConflictDetected",
                callback = function()
                    vim.notify("Conflict detected in " .. vim.fn.expand("<afile>"))
                end,
            })

            vim.api.nvim_create_autocmd("User", {
                pattern = "GitConflictResolved",
                callback = function()
                    vim.notify("Conflict resolved in " .. vim.fn.expand("<afile>"))
                end,
            })
        end,
    },
    {
        "f-person/git-blame.nvim",
        enabled = false,
        config = function()
            vim.cmd([[
            let g:gitblame_enabled = 1
            ]])
        end,
    },
    {
        "stevearc/dressing.nvim",
        event = "VeryLazy",
    },
    {
        "uga-rosa/ccc.nvim",
        cmd = { "CccPick", "CccConvert" },
    },
    {
        "mistweaverco/kulala.nvim",
        ft = "http",
        config = function()
            require("kulala").setup()

            vim.filetype.add({
                extension = {
                    ["http"] = "http",
                },
            })
        end,
    },
}
