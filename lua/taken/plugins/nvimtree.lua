--- @type LazyPluginSpec
return {
    "nvim-tree/nvim-tree.lua",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    enabled = false,
    init = function()
        vim.g.loaded_netrw = 1
        vim.g.loaded_netrwPlugin = 1
    end,
    config = function()
        local nvimtree = require("nvim-tree")

        nvimtree.setup({
            sync_root_with_cwd = true,
            view = {
                width = 30,
            },
            git = {
                ignore = false,
            },
            filters = {
                dotfiles = false,
                custom = {
                    "node_modules",
                    ".git",
                },
                exclude = {
                    ".gitignore",
                },
            },
        })

        vim.keymap.set("n", "<leader>n", ":NvimTreeToggle <CR>", { silent = true, desc = "Toggle nvim-tree" })
        vim.keymap.set("n", "<leader>e", ":NvimTreeFocus <CR>", { silent = true, desc = "Focus nvim-tree" })
        vim.keymap.set("n", "<leader>r", ":NvimTreeRefresh <CR>", { silent = true, desc = "Refresh nvim-tree" })
        vim.keymap.set("n", "<leader>tr", function()
            require("nvim-tree.api").tree.change_root(vim.fn.getcwd())
        end, { silent = true, desc = "Change root dir to current dir" })

        local autocmd = vim.api.nvim_create_autocmd
        local augroup = vim.api.nvim_create_augroup
        local neogitCmds = augroup("MyCustomNeogitEvents", { clear = true })

        autocmd("User", {
            pattern = "NeogitPushComplete",
            group = neogitCmds,
            callback = function()
                require("nvim-tree.api").tree.reload()
            end,
        })
        autocmd("User", {
            pattern = "NeogitPullComplete",
            group = neogitCmds,
            callback = function()
                require("nvim-tree.api").tree.reload()
            end,
        })
        autocmd("User", {
            pattern = "NeogitFetchComplete",
            group = neogitCmds,
            callback = function()
                require("nvim-tree.api").tree.reload()
            end,
        })
    end,
}
