--- @type LazyPluginSpec
return {
    "mikavilpas/yazi.nvim",
    event = "VeryLazy",
    config = function()
        require("yazi").setup({
            open_for_directories = false,
            keymaps = {
                show_help = "<f1>",
            },
        })

        vim.keymap.set("n", "<leader>fy", "<cmd>Yazi<cr>", { silent = true, desc = "Open yazi at the current file" })
        vim.keymap.set(
            "n",
            "<leader>fc",
            "<cmd>Yazi cwd<cr>",
            { silent = true, desc = "Open the file manager in nvim's working directory" }
        )
    end,
}
