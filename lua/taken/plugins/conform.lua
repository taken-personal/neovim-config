--- @type LazyPluginSpec
return {
    "stevearc/conform.nvim",
    event = { "BufReadPre", "BufNewFile" },
    config = function()
        local function checkPrettierConfig()
            local prettier_config = vim.fn.glob(".prettierrc*", false, 1)
            if prettier_config[1] == nil then
                return false
            end
        end

        local function dprintOrPrettier()
            if checkPrettierConfig() then
                return "prettier"
            else
                return "dprint"
            end
        end

        require("conform").setup({
            formatters_by_ft = {
                lua = { "stylua" },
                javascript = { dprintOrPrettier() },
                typescript = { dprintOrPrettier() },
                javascriptreact = { "rustywind", dprintOrPrettier() },
                typescriptreact = { "rustywind", dprintOrPrettier() },
                json = { dprintOrPrettier() },
                markdown = { dprintOrPrettier() },
                python = { "ruff_format" },
            },
            format_on_save = {
                timeout_ms = 500,
                lsp_format = "fallback",
            },
        })

        vim.api.nvim_create_user_command("Format", function(args)
            local range = nil
            if args.count ~= -1 then
                local end_line = vim.api.nvim_buf_get_lines(0, args.line2 - 1, args.line2, true)[1]
                range = {
                    start = { args.line1, 0 },
                    ["end"] = { args.line2, end_line:len() },
                }
            end
            require("conform").format({ async = true, lsp_format = "fallback", range = range })
        end, { range = true })
    end,
}
