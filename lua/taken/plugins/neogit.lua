--- @type LazyPluginSpec
return {
    "NeogitOrg/neogit",
    dependencies = "nvim-lua/plenary.nvim",
    cmd = "Neogit",
    keys = {
        { "<leader>gg", "<cmd>Neogit<cr>", desc = "Neogit" },
    },
    config = function()
        local neogit = require("neogit")

        neogit.setup({
            auto_refresh = true,
            remember_settings = true,
            disable_insert_on_commit = false,
            commit_popup = {
                kind = "split",
            },
            preview_buffer = {
                kind = "split",
            },
            popup = {
                kind = "split",
            },
            signs = {
                section = { "", "" },
                item = { "", "" },
                hunk = { "", "" },
            },
            integrations = {
                diffview = true,
            },
            sections = {
                untracked = {
                    folded = false,
                },
                unstaged = {
                    folded = false,
                },
                staged = {
                    folded = false,
                },
                stashes = {
                    folded = true,
                },
                unpulled = {
                    folded = true,
                    hidden = false,
                },
                unmerged = {
                    folded = false,
                    hidden = false,
                },
                recent = {
                    folded = true,
                },
            },
            mappings = {
                popup = {
                    ["p"] = "PushPopup",
                    ["P"] = "PullPopup",
                },
            },
        })
    end,
}
