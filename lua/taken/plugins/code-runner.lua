--- @type LazyPluginSpec
return {
    "CRAG666/code_runner.nvim",
    keys = {
        { "<leader>rc", ":RunCode<CR>", desc = "Run code" },
        { "<leader>rf", ":RunFile<CR>", desc = "Run file" },
        { "<leader>rt", ":RunFile tab<CR>", desc = "Run file in tab" },
        { "<leader>rp", ":RunProject<CR>", desc = "Run project" },
        { "<leader>rx", ":RunClose<CR>", desc = "Run close" },
        { "<leader>rrf", ":CRFiletype<CR>", desc = "CR filetype" },
        { "<leader>rrp", ":CRProjects<CR>", desc = "CR projects" },
    },
    config = function()
        require("code_runner").setup({
            filetype = {
                python = "python3 -u",
                typescript = "tsx",
                javascript = "node",
            },
        })
    end,
}
