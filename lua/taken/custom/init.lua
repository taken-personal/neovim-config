local prefs = require("taken.prefs")

vim.cmd("colorscheme " .. prefs.colorscheme)

local cursorcolumncolor = vim.fn.synIDattr(vim.fn.hlID("CursorColumn"), "bg")
if cursorcolumncolor ~= "" then
    vim.cmd("hi CursorLine guibg=" .. cursorcolumncolor)
end
