# My curent Neovim config

Inspired by [ThePrimeagen](https://github.com/ThePrimeagen/init.lua) and [Josean](https://github.com/josean-dev/dev-environment-files)

## Dependencies
- Neovim 0.9.5
- git
- fd

## Mason Dependencies
### Unix
- curl
- unzip
- tar
- gzip

### Windows
- pwsh or powershell
- git
- tar
- One of (7zip, peazip, winzip or WinRAR)

You can run `:Lazy sync` to install update and clean plugins
